<?php
# local or hosted?
if ($_SERVER['HTTP_HOST']=='localhost') {
  require ('includes/db_settings_local.php');
} else {
  require ('../../*****');
}

require ('includes/db_functions.php');

# html header
include ('includes/header.php');


# USER INPUT from GET

$error = '';

if(isset($_GET['word'])) { 

  $word = $_GET['word'];

  # clean input and escape apostrophe
  $cleanedword = preg_replace("/[^A-Za-z0-9\-']/", '', $word);
  $escapedword = preg_replace("/[']/", "\'", $cleanedword);


  # DATABASE
  # find word in the database and return 
  # vowel sound and stress pattern 

  $query1="
        SELECT DISTINCT sound, stress
        FROM words
        WHERE word='$escapedword';
        ";

  $result1=select_in_database($query1);

  if ($result1) {
    $sound  = $result1[0]['sound'];
    $stress = $result1[0]['stress'];


    # find all words with the same vowel and stress

    $query2="
          SELECT word
          FROM words
          WHERE sound='$sound'
          AND stress='$stress'
          AND word!='$escapedword';
          ";

    $result2=select_in_database($query2);
  } else {
    $error="No matching words found!";
  }
}

# FORM
?>

<form method="get">
<input type="text" name="word" value="<?php echo $word; ?>" />
<input type="submit" name="" value="Search" />
</form>
<hr>
<small><?php echo "$error";?></small><br>
<?php

# SHOW RESULTS
if (!empty($result2)) {

  $wordcounter = 1;
  $words = count($result2);

  echo "<p>";

  foreach ($result2 as $item) {
    echo $item['word'];

    # every word is followed by a comma, last word by a dot
    echo ($wordcounter < $words ? ', ' : '.</p>');
    $wordcounter++;
  }
}

