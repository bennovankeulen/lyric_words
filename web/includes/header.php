<!DOCTYPE html>
<html lang="nl">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="favicon.ico">
<title>
Lyric words
</title>

<link href='css/normalize.css' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/sakura.css" type="text/css">
</head>

<body>
<h1>Lyric words</h1>
<hr>
<h6>Find words with the same stress pattern and vowel sound</h6>

