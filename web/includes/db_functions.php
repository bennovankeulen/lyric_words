<?php 

/*
 * Verbind met database en voer query uit
 * Return resultaat in een associatieve array
 */ 

function select_in_database($query) {

  # verbinding met de database

  $mysqli_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

  if (!$mysqli_connection) {
    //echo "Error: Unable to connect to MySQL." . PHP_EOL;
    //echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    //echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    echo "Something went wrong while connecting with the database";
    exit;
  }

  # query uitvoeren
  $mysqli_result = mysqli_query($mysqli_connection, $query);

  # als er iets fout gaat met mysql
  if (!$mysqli_result) {
    echo 'Something went wrong' . mysql_error();
    exit;
  }

  # zet het resultaat van de query om in een associatieve array
  while($row = mysqli_fetch_assoc($mysqli_result)){
    $array[] = $row; 
  }
  return $array;
}


